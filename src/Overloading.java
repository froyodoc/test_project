public class Overloading {

    String laptop(){
        return "Laptop";
    }

    String laptop(String a){
        return a;
    }

    int laptop(int a){
        return 1;
    }

    int laptop(int a, String b){
        return 1;
    }

}
