import model.inheritance.Mobil;
import model.inheritance.MobilAvanza;

public class Main {

    public static void main(String args[]){
        String name = "Mahardika";
        String[] mahasiswa = {"Rini", "Aldi", "Acep", "Putra"};

        System.out.println(name);
        System.out.println(mahasiswa[2]);
        System.out.println(cetakArray(0,1));
        ucapSalam();
        ucapin("Apa kabar");
        System.out.println("Luas " + luasPersegi(5));
        System.out.println(luasKubus(10));


        Mobil mobil = new Mobil("Avanza", 4, 20.0);
        mobil.setRoda(10);
        System.out.println("Isi method roda : " + mobil.getRoda());

        Mobil mobilFerrari = new Mobil("Sport", 4, 200.0);
        Mobil mobilJeep = new Mobil("Offroad", 6, 150.0);

        System.out.println("model.inheritance.Mobil Ferrari");
        System.out.println("roda " + mobilFerrari.getRoda());
        System.out.println("type " + mobilFerrari.getType());
        System.out.println("kecepatan " + mobilFerrari.getKecepatan());

        System.out.println("");

        System.out.println("model.inheritance.Mobil Jeep");
        System.out.println("roda " + mobilJeep.getRoda());
        System.out.println("type " + mobilJeep.getType());
        System.out.println("kecepatan " + mobilJeep.getKecepatan());

        System.out.println("");

        MobilAvanza obj = new MobilAvanza("Deskripsi Mobil", "Type Avanza");
        System.out.println("Deskripsi : " + obj.getDeskripsi());
        System.out.println("Type : " + obj.getType());
        System.out.println("Kecepatan : " + obj.getKecepatan());
        System.out.println("Roda : " + obj.getRoda());
        obj.doMelaju(100.0);
        obj.superVariable("Avanza");

        System.out.println("");

        Implementasi imp = new Implementasi();
        String nama = "nama saya";
        String nim = "1234";

        imp.mBiodata();
        imp.mData(nama, nim);

    }

    //fungsi static akan membuat fungsi dapat dieksekusi langsung, tanpa harus membuat instansi objek dari class
    public static String cetakArray(int bariske, int kolomke){
        String[][]kontak = {
                {"lili", "08111"},
                {"Lala", "08122"},
                {"Maya", "08133"}
        };

        return kontak[bariske][kolomke];
    }

    //function tanpa parameter
    static void ucapSalam(){
        System.out.println("Selamat pagi");
    }

    //function dengan parameter
    static void ucapin(String ucapan){
        System.out.println(ucapan);
    }

    static int luasPersegi(int sisi){
        int luas = sisi * sisi;
        return luas;
    }

    static int luasKubus(int sisi){
        return 6 * luasPersegi(sisi);
    }
}
