public class GarbageCollectorClass {

    public void finalize(){
        System.out.println("Objek yang tidak terpakai sudah di bersihkan");
    }

    public static void main(String[] args){
        Runtime RT = Runtime.getRuntime();
        System.out.println("Jumlah memori awal " + RT.totalMemory());

        GarbageCollectorClass objek1 = new GarbageCollectorClass();
        GarbageCollectorClass objek2 = new GarbageCollectorClass();
        GarbageCollectorClass objek3 = new GarbageCollectorClass();

        objek1 = null;
        objek2 = null;

        System.out.println("Jumlah memori yang tersedia sebelun di gc : " + RT.freeMemory());
        System.gc();

        System.out.println("");

        System.out.println("Jumlah memori yang tersedia setelah di GC " + RT.freeMemory());
    }

}
