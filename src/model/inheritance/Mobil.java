package model.inheritance;

public class Mobil {

    public String type;
    private int roda;
    private Double kecepatan;

    public Mobil(){}

    public Mobil(String type, int roda, Double kecepatan){
        this.type = type;
        this.roda = roda;
        this.kecepatan = kecepatan;
    }

    //setter and getter
    public String getType(){
        return type;
    }

    public void setType(String type){
     this.type = type;
    }

    public int getRoda(){
        return roda;
    }

    public void setRoda(int roda){
        this.roda = roda;
    }

    public Double getKecepatan(){
        return kecepatan;
    }

    public void setKecepatan(Double kecepatan){
        this.kecepatan = kecepatan;
    }

    //setter getter end

    public void doMelaju(Double kecepatan){
        System.out.println("Melaju dengan kecepatan " + kecepatan);
    }

    public void doKlakson(){
        System.out.println("Klakson");
    }

    public void doBelok(String arah){
        System.out.println("Belok ke arah " + arah);
    }

    public double hitungKecepatan(Double kecepatan, int waktuDetik){
        return kecepatan/waktuDetik;
    }
}
